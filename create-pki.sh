
#pki_gnr_ca
vault secrets enable -path=pki_gnr_ca pki

vault secrets tune -max-lease-ttl=87600h pki_gnr_ca 

vault write -field=certificate pki_gnr_ca/root/generate/internal max_path_length=1 key_usage='' ext_key_usage='' common_name="Gengar Root CA" ou="GNR PKI" organization="Gengar Corp" country="GB" ttl=87600h key_bits=4096 > CA_cert.crt

vault write pki_gnr_ca/config/urls issuing_certificates="http://vault.gengar.com/v1/pki_gnr_ca/ca" crl_distribution_points="http://vault.gengar.com/v1/pki_gnr_ca/crl"

vault write pki_gnr_ca/config/crl expiry="4380h"

vault read pki_gnr_ca/crl/rotate

# Create a Role
vault write pki_gnr_ca/roles/Gengar allowed_domains="Gengar" allow_subdomains=false key_usage="" ext_key_usage="" ext_key_usage_oids="" ttl="4380h" max_ttl="4380h" ou="GNR PKI" organization="Gengar Corp"   country="GB"  enforce_hostnames=false allow_bare_domains=true


#pki_int_gnr_ca
vault secrets enable -path=pki_int_gnr_ca pki

vault secrets tune -max-lease-ttl=43800h pki_int_gnr_ca

vault write -format=json pki_int_gnr_ca/intermediate/generate/internal max_path_length=0 key_bits=4096 common_name="Gengar Issuing CA" ou="GNR PKI" organization="Gengar Corp" country="GB" | jq -r '.data.csr' > pki_int_gnr_ca.csr

vault write -format=json pki_gnr_ca/root/sign-intermediate max_path_length=0 key_bits=4096 common_name="Gengar Issuing CA" ou="GNR PKI" organization="Gengar Corp" country="GB" csr=@pki_int_gnr_ca.csr format=pem_bundle ttl="43800h" | jq -r '.data.certificate' > pki_int_gnr_ca.cert.pem

vault write pki_int_gnr_ca/intermediate/set-signed certificate=@pki_int_gnr_ca.cert.pem

vault write pki_int_gnr_ca/config/urls issuing_certificates="http://vault.gengar.com/v1/pki_int_gnr_ca/ca" crl_distribution_points="http://vault.gengar.com/v1/pki_int_gnr_ca/crl"

vault write pki_int_gnr_ca/config/crl expiry="4380h"

vault read pki_int_gnr_ca/crl/rotate


# Create a Roles
vault write pki_int_gnr_ca/roles/Issuer  allow_any_name=True server_flag=False client_flag=False allow_subdomains=false ou="Tech Center" organization="Gengar Corp" country="GB" key_usage="DigitalSignature, KeyEncipherment" use_csr_common_name=true ext_key_usage="" ext_key_usage_oids="" ttl="4380h" max_ttl="4380h"   enforce_hostnames=false allow_bare_domains=true


# Issue a certificate with Role
vault write -format=json pki_int_gnr_ca/issue/Issuer name="Test Certificate" common_name="Test Certificate" | jq -r '.data.certificate' | openssl x509 -noout -text 
